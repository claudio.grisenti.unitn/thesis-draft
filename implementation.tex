\section{Implementation}\label{implementation}
In this chapter technical details will be discussed in order to explain how the plugin works and why some decisions were made, describing each technology used in the implementation and its use.

The core technology used for the implementation is \textbf{Burp Suite}\cite{burp}, a suite of security testing tools popular in the penetration testing world. This suites include a wide variety of features and at the time of writing three versions are available with different capabilities and destined to different audiences:
\begin{description}
	\item[Community:]{this version is destined for hobbyists and researchers that need basic manual tools}
	\item[Professional:]{intended for professionals and bug bounty hunters who require advanced tools}
	\item[Enterprise:]{destined for businesses that need top tier protection and scalable tools}
\end{description}
Every version is available for every major OS (Windows, Mac and Linux) and uses java as programming language.

The main reason for choosing this applications is the possibility of creating custom plugins for it, giving the developer the possibility of using the already available feature to create new ones to expand the program. The languages possible to use to implement the plugin are java, ruby and python.

\subsection{User Interface}
\begin{figure}[H]
	\resizebox{\textwidth}{!}{
		\includegraphics{UIexample.png}}
	\caption{UI - example after all checks run}
	\label{fig:UIexample}
\end{figure}

An example of the GUI after all checks run can be found in figure \ref{fig:UIexample}. The text area on the top is used to insert the test track to guide the plugin trough the login phase and then on the landing page of the website. Below that portion you can find the GUI section which is used to show the user the result of the checks: both using buttons colored based on the tests outcome and a table containing a column for each \textbf{URL} which a vulnerability is found.

The available buttons are:
\begin{description}
	\item[Select Driver]{button to select the binary file for Selenium WebDriver}
	\item[Test Track]{button to initialize a check on the track used to run the checks. It also check what protocol(OAuth/OpenID) is used in the web app taken in consideration}
	\item[Use Chrome/Use Firefox]{selects which web browser to use(Only Chrome/Chromium/Firefox compatible)}
	\item[Run passive test]{it runs the passive checks}
	\item[Run active test]{it runs the active checks(some of them rely on the passive ones)}
	\item[Save Result]{saves the result available in the bottom table as JSON file. If a file with the same name already exists, it is \textbf{not} overwritten and the save process is aborted}
\end{description}

\subsection{Plugin Architecture}
At the beginning of this project we decided to expand an already available Burp plugin which featured some key components and implement the missing checks described in chapter \ref{sec:testcases}. The core structure has been maintained for its simplicity, modularity and possibility of creating new plugins destined to different protocols or standards.

The dependencies used in the base plugin are composed only of Selenium WebDriver \cite{selenium}, a framework which can be used to automate user interaction with web browsers, while in our expanded version of the plugin we added a library to manipulate JSON for OpenID's JWT.

What Selenium does is open a web browser, act on the commands received by the user and have Burp as HTTP proxy. This is done to ensure Burp manipulating the right traffic, which is the authentication and authorization of users using OAuth/OpenID.

In the source code there are many java files and many of them are simply interfaces required by Burp to include specific feature, some examples can be the possibility of redirecting traffic or creating custom logs.  The core files for the plugin to run are:
\begin{description}
	\item[BurpExtender.java:]{ the "main" class of the project. It handles all the methods needed from Burp and it interfaces it with all the other classes of the project. It also contains the section which manages the active tests.}
	\item[ExtensionGUI.java]{this class handles the entire GUI of the plugin, including the result printing and the commands input. It also include all the code performed by Selenium WebDriver, both the track check and execution}
	\item[TestTools.java]{a class container for all the utilities needed for both active and passive checks and application flow.}
	\item[VulnItem.java]{a simple class which represent the information about a found vulnerability.}
\end{description}

A complete list of all the source code files can be found in table \ref{table:javafiles}.

\begin{table}[H]
		\renewcommand{\arraystretch}{1}
	\resizebox{\textwidth}{!}{
	\begin{tabular}{ccc}
		\hline
		&&\\
		\textbf{BurpExtender.java}&IMessageEditor.java
&\textbf{ExtensionGUI.java}\\
		IMessageEditorTabFactory.java
&	IBurpCollaboratorClientContext.java&IMessageEditorTab.java
\\
		IBurpCollaboratorInteraction.java&IParameter.java
&	IBurpExtenderCallbacks.java\\
		IProxyListener.java
&IBurpExtender.java&IRequestInfo.java
\\
		IContextMenuFactory.java&IResponseInfo.java&IContextMenuInvocation.java\\
		IResponseKeywords.java
&ICookie.java&IResponseVariations.java
\\
		IExtensionHelpers.java&IScanIssue.java&IExtensionStateListener.java\\
		IScannerCheck.java
&IHttpListener.java&IScannerInsertionPoint.java
\\
		IHttpRequestResponse.java&IScannerInsertionPointProvider.java
&	IHttpRequestResponsePersisted.java\\
		IScannerListener.java
&	IHttpRequestResponseWithMarkers.java&IScanQueueItem.java
\\
		IHttpService.java&IScopeChangeListener.java
&	IInterceptedProxyMessage.java\\
		ISessionHandlingAction.java
&IIntruderAttack.java&ITab.java
\\
		IIntruderPayloadGeneratorFactory.java&ITempFile.java&IIntruderPayloadGenerator.java\\
		ITextEditor.java&IIntruderPayloadProcessor.java &	\textbf{TestTools.java}\\
		IMenuItemHandler.java&\textbf{VulnItem.java}&IMessageEditorController.java\\
		&&\\
		\hline
	\end{tabular}}
\caption{Source code java files}
\label{table:javafiles}
\end{table}

\subsection{Plugin flow}
Before displaying a more deep description of the plugin we need to define some terminology:
\begin{description}
	\item[Passive checks:]{checks which can operate in the background while the traffic passes through Burp and \textit{DO NOT} to need change any packet.}
	\item [Active checks:]{checks which need to modify some packets as they pass through Burp which means that each check needs to reiterate the entire test track.
	\item[State of the plugin:]{the plugin can be in 3 different states based on what button the user clicks. \textit{Test track} when the track is tested for correctness, \textit{Passive Test} when passive checks are run, \textit{Active test} when active tests are run }	
}
\end{description}
The flow of the plugin is relative simple. The class \textbf{BurpExtender.java} interfaces with Burp Suite to provide all the feature to read and manipulate HTTP packets. This class differentiate on the various cases in which the plugin can be (one excludes the others). This possibility is possible due to having an instance of the \textbf{ExtensionGUI.java} class that contains a variable indicating what the current state is. This variable is a simple Integer that can denote test track when is run, passive checks during this type of test and each of the available active checks.

The approach described was chosen because due to the active tests requiring to reiterate through the test track multiple times and the possibility of adding new ones: it can be done by simply creating a new integer denoting the check and iterating one more time.

\subsubsection{Test Track}
This test has been developed to check whether the test track inserted by the user is written correctly, with the right commands and syntax.
This is performed by Selenium by reading the track line by line and executing the commands. If any type of error is encountered the action is stopped and the user is notified by appending an error signal on the text area.

This test also examine what of the two protocols the website is using by storing all the traffic. After that, every packet is review for JWT, indicating the use of OpenID.
\begin{table}[H]
\begin{tabularx}{\textwidth}{p{0.7\textwidth}c}
	\textbf{Commands available in the test track} & \textbf{Types of indicators}\\
	open \textbar http://www.example.com \textbar & name\\
	type \textbar indicator=example \textbar example & class\\
	click \textbar indicator=example \textbar example & id\\
	& xpath\\
	& link\\
	\textbf{Test Track Example}&\\
	open \textbar http://www.example.com \textbar&\\
	click \textbar xpath=//*[@id="signin-options"]/div/div[1]/a[2]/span[2] \textbar
&\\
	type \textbar id=ap\_email \textbar email
&\\
	type \textbar id=ap\_password \textbar password
&\\
	click \textbar name=continue \textbar
&\\
	
\end{tabularx}
\caption{Test Track Description}
\end{table}

\subsubsection{Passive and active checks}
Regarding \textit{passive checks}, the flow of the plugin is a simple execution of the test track, scan of the traffic and decide whether the current packet should be or should be not stored to later perform tests.

Obviously not all the traffic during an attempted login should be tested, there is a specific time frame in which HTTP packets are used to transport OAuth/OpenID data: \textbf{TestTools.java} includes all the methods used to recognize when this particular time in the protocols flow starts and ends. 

If some packets are recognized as useful, they are stored in a list of HTTPRequestResponse object in BurpExtender's instance of ExtensionGUI which will be scanned when the traffic is stopped for each check implemented.

On the other hand, \textit{active tests} do not need any storing of traffic, they check in every packet whether any parameter or content in the body's message has to be modified or deleted and they differentiate between checks on the variable indicating the plugin state.

The way this plugin prints the result to the user is managed by the ExtensionGUI.java class and it works similarly between passive and active tests:
\begin{description}
	\item{Passive tests: there is a mapping between each check and a series of methods which receives from them a list of \textbf{VulnItem.java} and based on its size change the GUI(changing color to the specific button and adding the vulnerability found in the result table)}
	\item{Active tests: also here there is a mapping between check and methods but the difference is that they need a boolean value, set by Selenium when the traffic stops based on if the url of the current page contains errors}
\end{description}

\subsection{Implemented checks}
As described in chapter \ref{sec:testcases}, some the vulnerabilities and best practices can not be tested due to the rule set at the beginning of our project: the plugin has to work with as many web app as possible. This reason made impossible to implement the next vulnerabilities checks:
\begin{description}
	\item[Tokens expiration and lifetime]{the lifetime of an access token should be short, not allowing attackers to use old ones much time after it was issued. This is difficult to test because of the wide variety of providers.}
	\item[Authorization code lifetime]{similar to the previous one, authorization codes should be consumed shortly after its issue to obtain a valid access token. Testing whether a code can be used long after its issue can be troublesome due to the number of providers.}
	\item[Correct use of Refresh Token]{this tokens should be used shortly before the expiration of the access token and not abused. Testing whether a provider allows consuming a refresh token multiple times and long before is due is difficult due to the different API used by the different providers.}
\end{description}
The remaining test cases described in chapter \ref{sec:testcases} were implemented and founded working correctly.

\newpage